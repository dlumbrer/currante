# Currante: English as a programming language

Proof of concept of automatic generation of code from a specification.

The underlying idea of Currante is to show how a user could produce
working code by using only a natural language, first for the specification
of the code, then for refining a set of automatically produced tests.
The code is automatically built based on the specification and the tests,
and is refined until the tests pass.

All of the work is done by a LLM. Currently, the following models are
supported: GPT3.5-turbo (via the OpenAI API), CodeLlama, and WizardCoder
(via [ollama](https://ollama.ai/) in localhost).

Currently, Currante can only produce stateless functions, which given some
parameters, produce some result. The complete process is as follows:

(1) Users define the name, parameters, and return value of the function,
and its specification, all of it in plain English. When done, they ask for the
production of a testsuite, and an explanation of it in plain English
(both will be produced automatically by the LLM).

(2) Users read the description of the testsuite. They can ask for a new
description (with some advice on how to improve it), or for a new testsuite
(with some advice on how to improve it, by for example adding new tests,
removing some tests, or modifying some tests). When they are satisfied with the
description of the testsuite, they ask for the production of the function,
and the run of the testsuite on it.

(3) Users can check the result of running the testsuite. If it is "PASSED",
the goal is reached: the produced function passes all the tests in the testsuite.
If that is not the result, users can ask for a new version of the function.
For producing it, the LLM will first produce an explanation of the problems
with the testsuite run (by checking its output), with some advice on how to fix
the function to pass the tests. This explanation and advice will later be fed to
the LLM to produce the new version of the function (therefore, all of this process
is automatic). The user can repeat this process as much as needed, until the
testsuite result is "PASSED". In the process, the user can also modify the
specification, if needed, and the testsuite itself.

You can use an interactive command line interface (now deprecated), or a web-based
interface. The web-based interface is much more convenient, and easier
to use and understand.

## How to install

* Clone this repository.
* Install dependencies, specified in `requirements.txt`.
 It is recommended to do it in a virtual environment.

Before running any of the interfaces, set up your OpenAI API key,
usually by setting up the `OPENAI_API_KEY` in the shell where the
user interface script will be run, and/or launch an ollama instance,
depending on the models you intend to use. In the case of ollama, it
should have installed `codellama` and/or `wizardcoder:13b-python`.

Example:

```python
git clone https://gitlab.com/jgbarah/currante
cd currante
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
export OPENAI_API_KEY="..."
ollama pull codellama
ollama pull wizardcoder:13b-python
```

## How to run the web-based interface

The runnable script is `currante_ui.py`.

Example:

```python
python3 currante_ui
```

Then, open a web browser pointing to the URL provided by the script,
usually http://127.0.0.1:7860 .

## How to run the command line interface

**[Deprecated]**: it is very likely that this interface doesn't work anymore.
Use at your own risk.

The runnable script is `currante_cli.py`.
Running it with the argument `--help` shows
 the options it supports. It can use an example configuration
 file: `example_spec.py`. Your OpenAI API token is needed
 in an environment variable (there are some other options, 
 check the documentation of langchain).

Example:

```python
python3 currante_cli -c example_spec.toml
```

To run other specifications, create new files following
the template of 'example_spec.toml'. Yoy can also omit
the configuration file, and you will be asked for
the specification and other details of the function.

Some more details:

* The produced function will be in `produced_function.py`.
* The produced testsuite will be in `produced_test.py`.
* Using the option `--log`, a log will be shown in stdout, with
 some details of the procedure, including the intermediate
 files, answers produced by the LLM, etc.
* The log can be redirected to a file using `--logfile`.
* After every interation, if tests fail, user input is expected
 (this can be just hitting RETURN). This is to avoid too many
 requests to the OpenAI API: each iteration typically involves
 two of them.
* In some cases, it is convenient to produce more tests in
 the testsuite, before truing to produce the function. You
 can do that by starting an answer with "EXP", when asked
 to say how to fix the texts. When tests are good enough,
 just enter "OK".
´
## Using other models

Other models supported by [langchain](https://langchain.com/) should be
easy to support. In particular, models avaliable via the OpenAI API,
supported by ollama, should be really easy to support. Check the
`set_model` function in `currante.py`, and the `model_ddown` drop down menu
in `currante_ui.py`.

## Contributions
``
Suggestions are accepted: open an issue in this repository.
Merge requests are welcome. In particular, if you can make this
work with CodeLlama, or other LLMs suitable to run locally,
please comment how you did that, or produce a merge request.

## License

This software is distributed under the GPLv3. See complete text
in the LICENSE file.
