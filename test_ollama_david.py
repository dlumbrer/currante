from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain_community.llms import Ollama
from langchain.prompts import PromptTemplate
from langchain.chains import LLMChain
import unittest
import importlib.util

def execute_tests(code, tests_code, i):
    # Crea un módulo temporal
    module_name = 'temp_module' + str(i)
    spec = importlib.util.spec_from_loader(module_name, loader=None)
    module = importlib.util.module_from_spec(spec)
    
    # Reinicia el módulo
    module.__dict__.clear()
    
    # Ejecuta el código y luego los tests en el mismo módulo
    exec(code, module.__dict__)
    exec(tests_code, module.__dict__)
    
    # Crea una suite de tests
    suite = unittest.TestSuite()
    suite.addTests(unittest.defaultTestLoader.loadTestsFromTestCase(module.TestFibonacciReverse))
    
    # Ejecuta tests
    runner = unittest.TextTestRunner()
    result = runner.run(suite)
    
    # Obtener resultados
    total = result.testsRun
    passed = total - len(result.failures) - len(result.errors)
    failed = len(result.failures)
    errors = len(result.errors)
    
    # Obtener nombres de tests fallidos
    failed_test_names = []
    for test, failure in result.failures:
        failed_test_names.append(test.id().split('.')[-1])
    
    # Devuelve el resultado
    return {
        "raw": result,
        "total": total,
        "passed": passed,
        "failed": failed,
        "errors": errors,
        "failed_tests": failed_test_names
    }


    
model = "codellama:34b-instruct"
# model = "codellama:13b-instruct"

llm = Ollama(model=model,
             base_url='http://10.20.64.140:11434')

codellama_template = PromptTemplate.from_template(
    "<s>[INST] <<SYS>>\\n{system}\\n<</SYS>>\\n\\n{user}[/INST]")

codellama_prompt = codellama_template.format(
    system="""You are an expert Python programmer who writes simple, concise code.
    Provide your answer in Python.
    Your answer should start with a [PYTHON] tag and end with a [/PYTHON] tag.
    Your python uniitest should start with a [TEST] tag and end with a [/TEST] tag.
    The answer should be a single function, nothing else.
    Function name: fibonacci_reverse.
    """,
    user="""Your task is: Calculate and print the first X numbers of the Fibonacci sequence in reverse order. Please generate comprehensive test cases using unittest, the class name should be TestFibonacciReverse.
            Arguments: number of numbers to calculate.
            Returns: list with the first X numbers of the Fibonacci sequence in reverse order.
        """
)


times = 100
ok = 0
testok = 0
space = 0
for i in range(times):
    result = llm.invoke(codellama_prompt)
    print(result)
    if ('[PYTHON]' in result) and ('[/PYTHON]' in result):
        # I get the code only
        start = result.find("[PYTHON]")
        end = result.find("[/PYTHON]")
        code = result[start + len("[PYTHON]"):end]
        
        # Extract the tests only if python code has been defined
        if ('[TEST]' in result) and ('[/TEST]' in result):
            # I get the code only
            start = result.find("[TEST]")
            end = result.find("[/TEST]")
            tests = result[start + len("[TEST]"):end]
            testok += 1
            result_test = execute_tests(code, tests, i)
            print(f"TESTS -> Total: {result_test['total']}, Passed: {result_test['passed']}, Failed: {result_test['failed']}, Errors: {result_test['errors']}")
            
            print("Failed functions: ", result_test['failed_tests'])
            
        
        
        ok += 1
    elif not result:
        space += 1
    print(i+1, ok, testok, space)
print ("OK:", ok, "NOK:", times-ok, "TESTS:", testok)
print(result)


